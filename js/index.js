$(function()
            {
                $("[data-toggle='tooltip']").tooltip();
                $("[data-toggle='popover']").popover();
                $('.carousel').carousel({
                    interval: 5000
                });
                $('#contacto').on('show.bs.modal', function (e){
                    console.log('El modal se está mostrando');
                    $('#ContactoBn').prop('disabled', true);
                    $('#ContactoBn').removeClass('btn-info');
                    $('#ContactoBn').addClass('btn-outline-info');
                });

                $('#contacto').on('shown.bs.modal', function (e){
                    console.log('El modal se mostró');
                });

                $('#contacto').on('hide.bs.modal', function (e){
                    console.log('El modal se está ocultando');
                    $('#ContactoBn').prop('disabled', false);
                    $('#ContactoBn').removeClass('btn-outline-info');
                    $('#ContactoBn').addClass('btn-info');
                });

                $('#contacto').on('hidden.bs.modal', function (e){
                    console.log('El modal se ocultó');
                });
            });